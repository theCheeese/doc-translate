# setup
1. install tesseract + language packs you need
2. source ./venv/bin/activate
3. python -m pip install -r requirements.txt
4. make a file called auth.json with the following content: { "api_key": "<YOUR_API_KEY>"}. That's it.
# usage
python doc-translate.py FILE_PATH

Supports common image formats and pdf
