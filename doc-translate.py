import sys
import os.path
import json
try:
    from PIL import Image
except:
    import Image
import pytesseract
import requests
from pdf2image import convert_from_path

if len(sys.argv) == 1:
    print('need parameter')
    exit(1)

path = sys.argv[1]
if os.path.isfile(path) == False:
    print('cannot find file ' + path)
    exit(2)

print('reading ' + sys.argv[1] + '...')
images = list()
if path[-3:] == 'pdf':
    count = 0
    page_images = convert_from_path(path)
    for p in page_images:
        count += 1
        name = '/tmp/' + str(count) + '.jpg'
        p.save(name, 'JPEG')
        images.append(name)
else:
    images.append(path)

result = ''
for image in images:
    result += pytesseract.image_to_string(image)

with open('./auth.json') as f:
    auth = json.load(f)

translator_endpoint = 'https://api-free.deepl.com/v2/translate'

data = {
        'auth_key': auth['api_key'],
        'text': result,
        'target_lang': 'RU',
        'preserve_formatting': '1'
        }

response = requests.post(translator_endpoint, data=data)
print(response.json()['translations'][0]['text'])

#print(result)
